package com.boxshooting.game.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.boxshooting.game.enums.Direction;

public class WalkingAnimation {
    private static final float ANIMATION_INTERVAL = 0.07f;
    // Constant rows and columns of the sprite sheet
    private static final int IMAGE_HEIGHT = 64, IMAGE_WIDTH = 64;
    // Objects used
    private Animation<TextureRegion>[] walkAnimations = new Animation[4]; // Must declare frame type (TextureRegion)
    private TextureRegion[] standingFrames = new TextureRegion[4];

    // A variable for tracking elapsed time for the animation
    private float stateTime;

    public WalkingAnimation() {
        // Load the sprite sheet as a Texture
        Texture walkSheet = new Texture(Gdx.files.internal("bow_sprites.png"));

        // Use the split utility method to create a 2D array of TextureRegions. This is
        // possible because this sprite sheet contains frames of equal size and they are
        // all aligned.
        TextureRegion[][] tmp = TextureRegion.split(walkSheet,
                IMAGE_WIDTH,
                IMAGE_HEIGHT);

        for (int i = 0; i < 4; i++) {
            // Place the regions into a 1D array in the correct order, starting from the top
            // left, going across first. The Animation constructor requires a 1D array.
            TextureRegion[] walkFrames = new TextureRegion[8];
            int index = 0;
            for (int j = 0; j < 8; j++) {
                walkFrames[index++] = tmp[8 + i][j];
            }

            // Save a standing frame as well
            standingFrames[i] = walkFrames[0];
            // Initialize the Animation with the frame interval and array of frames
            walkAnimations[i] = new Animation<TextureRegion>(ANIMATION_INTERVAL, walkFrames);
        }
        // time to 0
        stateTime = 0f;
    }

    public TextureRegion getCurrentFrame(Direction direction, boolean walking) {
        stateTime += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time

        if(walking) {
            // Get current frame of animation for the current stateTime
            return walkAnimations[direction.ordinal()].getKeyFrame(stateTime, true);
        }
        return standingFrames[direction.ordinal()];
    }
}
