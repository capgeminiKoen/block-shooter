package com.boxshooting.game.entities.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boxshooting.game.constructs.Vector2Int;
import com.boxshooting.game.entities.Drawable;

public class Level implements Drawable {
    private int levelNumber;
    private int numberOfEnemies;
    private Map map;

    public Level(int levelNumber, int numberOfEnemies, Map map) {
        this.levelNumber = levelNumber;
        this.numberOfEnemies = numberOfEnemies;
        this.map = map;
    }

    public static Level createLevel(int levelNumber) {
        Level level = new Level(
                levelNumber,
                getEnemiesPerLevel(levelNumber),
                new Map(Vector2Int.of(getMapSizeForLevel(levelNumber), getMapSizeForLevel(levelNumber))));
        level.map.createRandomMap();
        return level;
    }

    private static int getEnemiesPerLevel(int levelNumber) {
        return 10 + levelNumber * levelNumber * 5;
    }

    private static int getMapSizeForLevel(int levelNumber) {
        return 10 + levelNumber * 2;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(int levelNumber) {
        this.levelNumber = levelNumber;
    }

    public int getNumberOfEnemies() {
        return numberOfEnemies;
    }

    public void setNumberOfEnemies(int numberOfEnemies) {
        this.numberOfEnemies = numberOfEnemies;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    @Override
    public void draw(SpriteBatch batch) {
        map.draw(batch);
    }
}
