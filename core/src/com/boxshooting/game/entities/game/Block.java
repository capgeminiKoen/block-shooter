package com.boxshooting.game.entities.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boxshooting.game.constructs.Vector2Int;
import com.boxshooting.game.entities.Drawable;
import com.boxshooting.game.enums.BlockType;

public class Block implements Drawable {
    private Vector2Int position;
    private BlockType blockType;

    public Block(Vector2Int position, BlockType blockType) {
        this.position = position;
        this.blockType = blockType;
    }

    public Vector2Int getPosition() {
        return position;
    }

    public void setPosition(Vector2Int position) {
        this.position = position;
    }

    public BlockType getBlockType() {
        return blockType;
    }

    public void setBlockType(BlockType blockType) {
        this.blockType = blockType;
    }

    @Override
    public void draw(SpriteBatch batch) {
        if(blockType.getTexture() != null) {
            batch.draw(blockType.getTexture(), position.getX(), position.getY());
        }
    }
}
