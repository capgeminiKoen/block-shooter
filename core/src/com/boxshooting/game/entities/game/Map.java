package com.boxshooting.game.entities.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boxshooting.game.constructs.Vector2Int;
import com.boxshooting.game.entities.Drawable;
import com.boxshooting.game.enums.BlockType;

import java.util.Random;

public class Map implements Drawable {
    private Vector2Int size;
    private Block[][] blocks;
    public static final int BLOCK_SIZE = 64;

    public Map(Vector2Int size) {
        this.size = size;
        this.blocks = new Block[size.getX()][size.getY()];
    }

    public void createRandomMap() {
        Random random = new Random();
        for (int j = 0; j < blocks.length; j++){
            for (int i = 0; i < blocks[j].length; i++) {
                blocks[j][i] = new Block(Vector2Int.of(i * BLOCK_SIZE, j * BLOCK_SIZE), BlockType.values()[random.nextInt(BlockType.values().length)]);
            }
        }
    }

    @Override
    public void draw(SpriteBatch batch) {
        for (Block[] block : blocks) {
            for (Block aBlock : block) {
                aBlock.draw(batch);
            }
        }
    }
}
