package com.boxshooting.game.entities.projectiles;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.boxshooting.game.enums.Direction;
import com.boxshooting.game.helpers.TextureCreator;

public class Arrow extends Projectile {

    public Arrow(Direction direction, Vector2 position) {
        super(direction, position);

        speed = 750f;
        texture = TextureCreator.createRectangleTexture(64, 64, 0, 2, 0, 0.75f);
    }
}
