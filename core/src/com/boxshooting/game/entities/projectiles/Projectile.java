package com.boxshooting.game.entities.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boxshooting.game.entities.living.BaseEntity;
import com.boxshooting.game.enums.Direction;

public abstract class Projectile extends BaseEntity {
    protected float speed = 250f;
    protected float damage  = 20;
    private Direction direction;

    public Projectile(Direction direction, Vector2 position,  float speed, float damage) {
        this(direction, position);
        this.damage = damage;
        this.speed = speed;
    }

    public Projectile(Direction direction, Vector2 position) {
        this.direction = direction;
        this.position = position;
    }

    @Override
    public void draw(SpriteBatch batch) {
        updatePosition();
        super.draw(batch);
    }

    protected void updatePosition(){
        float movement = speed * Gdx.graphics.getDeltaTime();
        switch(direction) {
            case EAST:
                move(movement, 0);
                break;
            case WEST:
                move(-movement, 0);
                break;
            case NORTH:
                move(0, movement);
                break;
            case SOUTH:
                move(0, -movement);
                break;
        }
    }
}
