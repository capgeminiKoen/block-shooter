package com.boxshooting.game.entities.living;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boxshooting.game.GameManager;
import com.boxshooting.game.animations.WalkingAnimation;
import com.boxshooting.game.entities.projectiles.Arrow;
import com.boxshooting.game.enums.Direction;

public class Character extends BaseEntity {

    public static final int CHARACTER_STARTING_HEALTH = 100;
    private WalkingAnimation walkingAnimation = new WalkingAnimation();
    private float movementSpeed = 200f;
    private Direction facingDirection = Direction.EAST;
    private boolean walking = false;

    public Character() {
        super();
        this.health = CHARACTER_STARTING_HEALTH;
    }

    @Override
    public void draw(SpriteBatch batch) {
        // Handle input
        handleInput();

        batch.draw(walkingAnimation.getCurrentFrame(facingDirection, walking), position.x, position.y);
    }

    private void handleInput() {
        Vector2 movement = new Vector2(0,0);
        walking = false;
        float actualMovementSpeed = Gdx.graphics.getDeltaTime() * movementSpeed;
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            movement.x = actualMovementSpeed;
            facingDirection = Direction.EAST;
            walking = true;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            movement.x = -actualMovementSpeed;
            facingDirection = Direction.WEST;
            walking = true;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            movement.y = actualMovementSpeed;
            facingDirection = Direction.NORTH;
            walking = true;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            movement.y = -actualMovementSpeed;
            facingDirection = Direction.SOUTH;
            walking = true;
        }

        if(walking) {
            move(movement);
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            fire();
        }
    }

    private void fire() {
        GameManager.addProjectile(new Arrow(facingDirection, position.cpy()));
    }
}
