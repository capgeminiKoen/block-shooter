package com.boxshooting.game.entities.living;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.boxshooting.game.constructs.Vector2Int;
import com.boxshooting.game.entities.Drawable;

public abstract class BaseEntity implements Drawable {
    protected int health;
    protected Vector2 position;
    protected Texture texture;

    public BaseEntity() {
        position = new Vector2(0,0);
    }

    public void move(int x, int y) {
        move((float)x, (float)y);
    }

    public void move(float x, float y) {
        position.add(x, y);
    }

    public void move(Vector2Int movementVector) {
        move(movementVector.getX(), movementVector.getY());
    }

    public void move(Vector2 movementVector) {
        move(movementVector.x, movementVector.y);
    }

    public void giveDamage(int amount){
        this.health -= amount;
        checkForDeath();
    }

    private void checkForDeath() {
        if(this.health <= 0) {
            this.die();
        }
    }

    protected void die() {

    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(texture, position.x, position.y);
    }
}
