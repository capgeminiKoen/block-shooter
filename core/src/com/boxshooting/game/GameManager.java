package com.boxshooting.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.boxshooting.game.constructs.Vector2Int;
import com.boxshooting.game.entities.Drawable;
import com.boxshooting.game.entities.game.Level;
import com.boxshooting.game.entities.game.Map;
import com.boxshooting.game.entities.living.Character;
import com.boxshooting.game.entities.projectiles.Projectile;
import com.boxshooting.game.enums.GameState;
import com.boxshooting.game.enums.MenuState;
import com.boxshooting.game.helpers.TextureCreator;

import java.util.ArrayList;
import java.util.List;

public class GameManager implements Drawable {
    private static final float PREGAME_STARTUP_TIME = 1.5f;
    private static final float PREGAME_COUNTDOWN_TIME = 3.0f;
    private static final float START_GAME_MESSAGE_TIME = 1.5f;
    private static final float TOTAL_PREGAME_TIME =
            PREGAME_STARTUP_TIME +
            PREGAME_COUNTDOWN_TIME;
    private Character character;
    private static List<Projectile> projectiles = new ArrayList<Projectile>();
    private static float gameTimer;
    private static GameState gameState = GameState.GAME_NOT_STARTED;
    private static MenuState menuState = MenuState.MAIN_MENU;
    private static Level currentLevel;
    private static int currentLevelIndex = 0;

    public GameManager() {
        gameTimer = -TOTAL_PREGAME_TIME;
        character = new Character();
        startGame();
    }

    public static GameState getGameState() {
        return gameState;
    }

    public static MenuState getMenuState() {
        return menuState;
    }

    @Override
    public void draw(SpriteBatch batch) {
        currentLevel.draw(batch);
        for (Projectile p : projectiles) {
            p.draw(batch);
        }
        // Draw character last
        character.draw(batch);
        if(gameState != GameState.IN_MENU && gameState != GameState.GAME_PAUSED) {
            gameTimer += Gdx.graphics.getDeltaTime();
        }
        if(gameState == GameState.GAME_COUNTING_DOWN) {
            drawCountdownTimer(batch);
        }
        handleGameStates();
    }

    private void drawCountdownTimer(SpriteBatch batch) {
        int currentTime = (int)(gameTimer * -1);
        TextureCreator.font.draw(batch, Integer.toString(currentTime), 10, 10);
    }

    private void handleGameStates() {
        if(gameState == GameState.GAME_COUNTING_DOWN && gameTimer >= 0f) {
            gameState = GameState.GAME_RUNNING;
        }
    }

    public static void addProjectile(Projectile projectile) {
        projectiles.add(projectile);
    }

    private static void startGame () {
        currentLevel = Level.createLevel(currentLevelIndex);
        gameState = GameState.GAME_COUNTING_DOWN;
    }
}
