package com.boxshooting.game.constructs;

public class Vector2Int {
    private int x;
    private int y;

    public Vector2Int(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Vector2Int of(int x, int y){
        return new Vector2Int(x, y);
    }
    public static Vector2Int Zero() {
        return new Vector2Int(0, 0);
    }

    public Vector2Int() {
        this(0,0);
    }

    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }

    public void move(Vector2Int movement) {
        this.move(movement.x, movement.y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
