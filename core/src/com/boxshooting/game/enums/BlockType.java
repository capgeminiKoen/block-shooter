package com.boxshooting.game.enums;

import com.badlogic.gdx.graphics.Texture;
import com.boxshooting.game.entities.game.Map;
import com.boxshooting.game.helpers.TextureCreator;

public enum BlockType {
    EMPTY(null),
    WALL(TextureCreator.createRectangleTexture(Map.BLOCK_SIZE, Map.BLOCK_SIZE, 0.3f, 0.3f, 0.3f, 1));
    private Texture texture;

    BlockType(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
};