package com.boxshooting.game.enums;

public enum Direction {
    NORTH,
    WEST,
    SOUTH,
    EAST
}
