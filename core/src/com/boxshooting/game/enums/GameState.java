package com.boxshooting.game.enums;

public enum GameState {
    GAME_PAUSED,
    GAME_OVER,
    GAME_RUNNING,
    GAME_NOT_STARTED,
    GAME_SHOW_SPLASH,
    GAME_COUNTING_DOWN,
    IN_MENU
}
