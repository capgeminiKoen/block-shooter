package com.boxshooting.game.helpers;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class TextureCreator {
    public static final BitmapFont font = new BitmapFont();
    public static Texture createRectangleTexture(int xSize, int ySize, float r, float g, float b, float a) {
        Pixmap pixmap = new Pixmap( xSize, ySize, Pixmap.Format.RGBA8888 );
        pixmap.setColor( r, g, b, a );
        pixmap.fillRectangle(0,0,xSize,ySize);
        Texture texture =new Texture( pixmap );
        pixmap.dispose();
        return texture;
    }
}
